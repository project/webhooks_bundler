<?php

namespace Drupal\webhooks_bundler\Queue;

use Drupal\webhooks\Entity\WebhookConfig;
use Drupal\webhooks\Webhook;

final class BundledQueueItem {

  private int $lastSent = 0;

  private int $lastPush = 0;

  private int $triggerInterval = 60 * 10;

  private WebhookConfig $webhookConfig;

  private Webhook $webhook;

  public function update(
    WebhookConfig $webhook_config,
    Webhook $webhook
  ): void {
    $this->webhookConfig = $webhook_config;
    $this->webhook = $webhook;
    $this->lastPush = time();
  }

  public function setTriggerInterval(int $triggerInterval): void {
    $this->triggerInterval = $triggerInterval;
  }

  public function shouldSend(): bool {
    return ($this->lastPush > $this->lastSent)
      && ($this->lastSent + $this->triggerInterval < time());
  }

  public function setLastSent(int $lastSent): void {
    $this->lastSent = $lastSent;
  }

  public function getWebhookConfig(): WebhookConfig {
    return $this->webhookConfig;
  }

  public function getWebhook(): Webhook {
    return $this->webhook;
  }

}