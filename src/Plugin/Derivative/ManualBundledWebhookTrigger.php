<?php

namespace Drupal\webhooks_bundler\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\webhooks\Entity\WebhookConfig;

final class ManualBundledWebhookTrigger extends DeriverBase {

  public function getDerivativeDefinitions($base_plugin_definition): array {
    return array_map(function(WebhookConfig $config) use ($base_plugin_definition) {
      return [
          'title' => $config->label(),
          'route_name' => 'webhooks_bundler.trigger.manual',
          'route_parameters' => ['webhook_id' => $config->id()],
        ] + $base_plugin_definition;
    }, WebhookConfig::loadMultiple());
  }

}