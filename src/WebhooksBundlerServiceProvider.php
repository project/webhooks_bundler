<?php

namespace Drupal\webhooks_bundler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

final class WebhooksBundlerServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container): void {
    if ($container->hasDefinition('webhooks.service')) {
      $container
        ->getDefinition('webhooks.service')
        ->setClass(WebhooksService::class);
    }
  }

}