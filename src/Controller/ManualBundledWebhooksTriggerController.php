<?php

namespace Drupal\webhooks_bundler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\State\StateInterface;
use Drupal\webhooks_bundler\WebhooksService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

final class ManualBundledWebhooksTriggerController extends ControllerBase {

  public function __construct(
    private readonly StateInterface $state,
    private readonly WebhooksService $webhookService,
  ) {}

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('state'),
      $container->get('webhooks.service'),
    );
  }

  public function __invoke(string $webhook_id): RedirectResponse {
    /** @var \Drupal\webhooks_bundler\Queue\BundledQueueItem $queue_item */
    $queue_item = $this->state->get("webhooks_bundler_queue:$webhook_id");
    if (is_null($queue_item)) {
      $this->messenger()->addError('Bundled webhook not found.');
      return $this->redirect('<front>');
    }

    // Force a trigger.
    $queue_item->setLastSent(0);

    $this->webhookService->send(
      $queue_item->getWebhookConfig(),
      $queue_item->getWebhook()
    );

    $this->messenger()->addMessage($this->t(
      "Bundled webhook '%webhook%' has been triggered.",
      ['%webhook%' => $queue_item->getWebhookConfig()->label()]
    ));

    return $this->redirect('<front>');
  }

}