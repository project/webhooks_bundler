<?php

namespace Drupal\webhooks_bundler;

use Drupal\webhooks\Entity\WebhookConfig;
use Drupal\webhooks\Webhook;
use Drupal\webhooks\WebhooksService as WehooksServiceBase;
use Drupal\webhooks_bundler\Queue\BundledQueueItem;

final class WebhooksService extends WehooksServiceBase {

  public function send(WebhookConfig $webhook_config, Webhook $webhook): void {
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = \Drupal::service('state');

    $queue_item = $state->get(
      "webhooks_bundler_queue:{$webhook_config->id()}"
    ) ?? new BundledQueueItem();

    $queue_item->update($webhook_config, $webhook);
    if ($queue_item->shouldSend()) {
      parent::send($webhook_config, $webhook);
      $queue_item->setLastSent(time());
    }

    $state->set("webhooks_bundler_queue:{$webhook_config->id()}", $queue_item);
  }

}
